#!/bin/bash

GREEN='\033[0;32m'
NC='\033[0m'

# Une petite fonction pour faire de beaux messages colorés !
pretty_log() {
    echo -e "${GREEN}$1${NC} 😊" > /dev/stderr
}

# Justement, un petit message tout beau... puis on attend 2 secondes pour que l'utilisateur ait le temps de le lire.
pretty_log "Début de l'installation de Peertube." && sleep 2

pretty_log "Installation des dépendances apt nécessaires." && sleep 2
apt-get update # Mise à jour du gestionnaire de paquets apt
apt-get install -y curl sudo unzip vim nginx ffmpeg postgresql postgresql-contrib openssl g++ make redis-server git cron wget

pretty_log "Installation de NodeJS" && sleep 2
wget -qO- https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt-get install -y nodejs
npm install --global yarn

pretty_log "Installation de Python3" && sleep 2
sudo apt-get install -y python3-dev python-is-python3

pretty_log "Création d'un utilisateur Peertube"
useradd -m -r -d /var/www/peertube -s /bin/bash -p peertube peertube
passwd peertube
chmod g+r,o+x /var/www/peertube

pretty_log "Installation de Peertube" && sleep 2
cd /var/www/peertube/
VERSION=$(curl -s https://api.github.com/repos/chocobozzz/peertube/releases/latest | grep tag_name | cut -d '"' -f 4)
pretty_log "Le dernière version de Peertube est : $VERSION" && sleep 2

pretty_log "Création des dossiers config, storage et versions nécessaires à Peertube" && sleep 2
sudo -u peertube mkdir config storage versions
sudo -u peertube chmod 750 config/

cd /var/www/peertube/versions
sudo -u peertube wget -q "https://github.com/Chocobozzz/PeerTube/releases/download/${VERSION}/peertube-${VERSION}.zip"
sudo -u peertube unzip -q peertube-${VERSION}.zip
sudo -u peertube rm peertube-${VERSION}.zip

cd /var/www/peertube
sudo -u peertube ln -s versions/peertube-${VERSION} ./peertube-latest
cd ./peertube-latest && sudo -H -u peertube yarn install --production --pure-lockfile

pretty_log "Création des fichiers de configuration"
cd /var/www/peertube
sudo -u peertube cp peertube-latest/config/default.yaml config/default.yaml

sudo -u peertube cp peertube-latest/config/production.yaml.example config/production.yaml

pretty_log "Edition des fichiers de configuration"
wget "https://gitlab.com/tlg-teach/peertube/-/raw/main/nginx.conf?inline=false" -O /etc/nginx/sites-available/peertube

echo -n "Entrez le nom de domaine de votre instance Peertube (ou son IP) : "

machine_ip=$(hostname -I | awk '{print $1}')
while true; do
    read DOMAIN
    if [ "$DOMAIN" == "$machine_ip" ]; then
        pretty_log "L'IP saisie est correcte."
        break
    else
        pretty_log "L'IP semble incorrecte. Veuillez vérifier."
    fi
done

sed -i "s/WEBSERVER_HOST/$DOMAIN/g" /etc/nginx/sites-available/peertube
sed -i "s/PEERTUBE_HOST/127.0.0.1:9000/g" /etc/nginx/sites-available/peertube

pretty_log "Activation du site nginx et redémarrage du service nginx" && sleep 2
ln -s /etc/nginx/sites-available/peertube /etc/nginx/sites-enabled/peertube
systemctl restart nginx.service

pretty_log "Configuration du service SystemD" && sleep 2
cp /var/www/peertube/peertube-latest/support/systemd/peertube.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable peertube

pretty_log "Installation automatisée terminée" && sleep 1
pretty_log "Éditez le fichier de configuration de Peertube /var/www/peertube/config/production.yaml pour finir la configuration."
echo "Vous devez adapter au moins :"
echo "  - La section 'webserver'"
echo "  - La section 'secrets'"
echo "  - La section 'database'"

pretty_log "Une fois cela fait, démarrez le service avec systemctl start peertube"

exit 0
