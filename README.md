# Peertube

Ce repo contient un [script d'installation](install.sh) de Peertube pour le projet du module Cloud de la licence pro SAN. Ce script est conçu pour être exécuté sur Debian 12.

## Utilisation

Télécharger le script :

```bash
wget https://gitlab.com/tlg-teach/peertube/-/raw/main/install.sh -O install_script.sh
```

Lui ajouter les droits en exécution :

```bash
chmod +x install_script.sh
```

L'exécuter :

```bash
./install_script.sh
```

**Il vous est vivement conseillé de prendre le temps de regarder le détail du script et de le comprendre.**

## Quelques commandes utiles

Une fois le script exécuté avec succès voici quelques commandes utiles.

Démarrer le service peertube :

```bash
systemctl start peertube.service
```

Consulter les logs du service peertube :

```bash
journalctl -feu peertube
```
